import java.util.ArrayList;
/**
 * Write a description of class Item here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Item
{
    // instance variables - replace the example below with your own
    private String itemName;
    public String itemDescription;
    public int itemWeight;

    /**
     * Constructor for objects of class Item
     */
    public Item(String itemName, String itemDescription, int itemWeight)
    {
       this.itemDescription = itemDescription;
       this.itemWeight = itemWeight;
       this.itemName = itemName;
    }

    public String itemDescription()
    {
        return itemDescription;
    }
    
    public int itemWeight()
    {
        return itemWeight;
    }
    
    public String itemName()
    {
        return itemName;
    }
}
