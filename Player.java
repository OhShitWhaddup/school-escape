import java.util.ArrayList;
/**
 * Write a description of class Player here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Player
{
    // instance variables - replace the example below with your own
    private Room currentRoom;
    private ArrayList<Item> inventory;
     

    public Player()
    {
        inventory = new ArrayList<>();
    }
    
    /**
     * Constructor for objects of class Player
     */
    public void Player()
    {
        System.out.println(currentRoom.getLongDescription());
        
    }
    
    public void takeItem(Command command)
    {
        if(!command.hasSecondWord()) {
            System.out.println("What do you want to take?");
            return;
        } 
        else {
            System.out.println("You have picked up a " + command.getSecondWord());
        }
    }
    
    
    
    public void addItemToInventory(Item itemToAdd)
    {
        inventory.add(itemToAdd);
    }

    /**
     * An example of a method - replace this comment with your own
     *
     * @param  y  a sample parameter for a method
     * @return    the sum of x and y
     */
    
}
